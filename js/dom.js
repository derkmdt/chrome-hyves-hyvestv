window.addEventListener('DOMContentLoaded', function(){
        if(location.href.indexOf("youtube.com") != "-1") {
                if(document.getElementById("watch-actions-right") || document.getElementById("watch-bar-channel") || document.getElementById("watch7-content")) {
                        var meta_tags = document.getElementsByTagName("meta");
                        for (var i = 0; i < meta_tags.length; i ++) {
                                if (meta_tags[i].getAttribute("name") === "title") {
                                        var title_site = "%20";
                                        title_site = meta_tags[i].getAttribute("content");
                                        title_site = encodeURIComponent(title_site).replace(/!/g,'%21').replace(/'/g,'%60').replace(/\(/g,'%28').replace(/\)/g,'%29').replace(/\*/g,'%2A').replace(/\,/g,'%2C').replace(/\./g,'%2E').replace(/%20/g,' ');
                                }
                        }
                        var skey = "";
                        var links = document.getElementsByTagName("link");
                        for (var i = 0; i < links.length; i ++) {
                                if (links[i].getAttribute("rel") === "canonical") {
                                        var canonical = "http://www.youtube.com"+links[i].getAttribute("href")
                                        var regexS = "[\\?&]v=([^&#]*)";
                                        var regex = new RegExp(regexS);
                                        skey = regex.exec(canonical);
                                }
                        }
                        if(skey[1]) {
                                var newP = document.createElement("div");
                                newP.innerHTML = '<a href="javascript:window.open(\'http://www.hyves-share.nl/button/gadget/?title=' + title_site + '&html='+ 'http://www.youtube.com/v/' + skey[1] +'\',\'sharepopuptip\',\'toolbar=0,status=0,width=500,height=550,scrollbars=yes\')" target="_blank"><img src="' + chrome.extension.getURL("img/hyvestv_button.png") + '" style="border:0px;padding:0px;margin:0px;" border="0"></a>'; 
                                var p2 = "";
                                if(document.getElementById("watch-actions-right")) {
                                        newP.style.cssFloat = 'right';
                                        newP.style.margin = '2px 5px 0 0';
                                        p2 = document.getElementById("watch-actions-right");
                                        p2.parentNode.insertBefore(newP, p2.nextSibling);
                                } else if (document.getElementById("watch-bar-channel")) {							
										newP.style.position = "absolute";
										newP.style.top = '6px';
										newP.style.left = '680px';
										p2 = document.getElementById("watch-bar-channel");
                                        p2.parentNode.insertBefore(newP, p2.nextSibling);
                                } else if (document.getElementById("watch7-views-info")) {							
										newP.style.position = "absolute";
										newP.style.top = '27px';
										newP.style.left = '240px';
										p2 = document.getElementById("watch7-views-info");
                                        p2.parentNode.insertBefore(newP, p2.nextSibling);
                                }
                        }
                }
    } else {
        function getCollection(tagname) {
            var all = document.getElementsByTagName(tagname);
            if(all.length == 0) return false;
                for(i=0;i<all.length;i++) {
                    if(all[i].src != '' || all[i].src != 'undefined') {
                        if(all[i].src.indexOf("youtube.com/v/") != "-1" || all[i].src.indexOf("youtube.com/embed/") != "-1" || (all[i].src.indexOf("s.ytimg.com/yt/")  != "-1" && all[i].src.indexOf(".swf") == "-1")) {
                        var surl = all[i].src.replace(/\?.*/, "")
                        var newP = document.createElement("div");
                        newP.innerHTML = '<div style="margin:5px 0 5px 0;"><a href="javascript:window.open(\'http://www.hyves-share.nl/button/gadget/?title=%20&html='+ surl +'\',\'sharepopuptip\',\'toolbar=0,status=0,width=500,height=550,scrollbars=yes\')" target="_blank"><img src="' + chrome.extension.getURL("img/hyvestv_button.png") + '" style="border:0px;padding:0px;margin:0px;" border="0"></a></div>'; 
                        var p2 = all[i];
                        p2.parentNode.insertBefore(newP, p2.nextSibling);
                    }
                }
            }
            return true;
        }
        getCollection('embed');
        getCollection('iframe');
        getCollection('video');
    }
},false)